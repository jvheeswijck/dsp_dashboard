from flask import Flask, render_template, request, send_from_directory, jsonify
import os, sys
import csv
import json
import pandas as pd
import numpy as np
import datetime as dt
from io import StringIO

# App config.
DEBUG = True
app = Flask(__name__)
app.config.from_object(__name__)
app.jinja_env.globals.update(zip=zip)
app.config['SECRET_KEY'] = '7d441f27d441f27567d441f2b6176a'
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1

# with open('layout_elements/general_elements.txt', 'r') as f:
#     general_nav = list(map(str.strip, f.readlines()))

# with open('layout_elements/tour_elements.txt', 'r') as f:
#     tour_nav = list(map(str.strip, f.readlines()))

with open('python/mappings/tour_abbr.json', 'r') as f:
    tour_abbr = json.load(f)

with open('python/mappings/tour_bar_order.json', 'r') as f:
    tour_order_json = json.load(f)
    tour_order_list = [k for k,v in tour_order_json.items()]

with open('python/mappings/num_to_tour.json', 'r') as f:
    num_to_tour = json.load(f)

with open('python/mappings/num_to_lang.json', 'r') as f:
    num_to_lang = json.load(f)

with open('python/mappings/tab_to_col.json', 'r')  as f:
    tab_to_col = json.load(f)

with open('python/mappings/langAB_to_lang.json', 'r') as f:
    expand_lang =  json.load(f)

with open('info_cards/general_tab.json', 'r') as f:
    gen_info = json.load(f)

with open('info_cards/tour_tab.json', 'r') as f:
    tour_info = json.load(f)

# Data Processing Code
df = pd.read_csv('static/data/all_data.csv')
df.date = df.date = pd.to_datetime(df.date, format="%Y%m%d")
df.tour_switch = df.tour_switch.map(lambda x: np.array(list(map(int,x.split()))))

min_date = df.date.min()
max_date = df.date.max()

# Media Stats   
df_media = pd.read_csv('static/data/tour_media_stats.csv')

##### SERVER CODE ######

server_data = "http://localhost:5000/data"

@app.route("/", methods=['POST', 'GET'])
def home():
    return render_template('general_metrics.html', first_date=min_date, last_date=max_date)

@app.route("/general", methods=['POST', 'GET'])
def general():
    # data_path = server_data + "popularity.csv"
    # data_path = server_data + "?p={}\&lang={}\&date={}".format("g", "1", "200101")
    # print(data_path)
    return render_template('general_metrics.html', first_date=min_date, last_date=max_date)

@app.route('/tours', methods=['POST', 'GET'])
def tours():
    try:
        tour_index = request.args['tour_index']
    except:
        tour_index = 0
    return render_template('tour_metrics.html', current_tour=tour_index, first_date=min_date, last_date=max_date)

# Handle data requests
@app.route('/data')
def provide_data():

    print("Recieving Request")
    print(request.args)

    # Handle Min-Max DateRange
    if request.args['g'] == 'date_range':
        dates = {'first': min_date.strftime("%m/%d/%Y"), 'last': max_date.strftime("%m/%d/%Y")}
        return jsonify(dates)

    # Handle Card Info
    if request.args['g'] == 'info':
        if request.args['page'] == 'gen':
            return jsonify(gen_info)
        elif request.args['page'] == 'tour':
            return jsonify(tour_info)

    if request.args['g'] == 'gen_data':
        tab = request.args['tab']
        lang = request.args['lang']

        # Aggregate barcharts
        if int(tab) in [0, 1, 2, 3]:
            frame = filter_bar(tab, lang, request.args)
            return prepare_csv(frame)

    if request.args['g'] == 'pie':
        tour_filter = num_to_tour[request.args['tour_index']]
        frame = filter_pie(tour_filter, request.args)
        return prepare_csv(frame)

    if request.args['g'] == 'stats':
        stats = filter_stats(request.args)
        return jsonify(stats)

    if request.args['g'] == 'audio_stats':
        stats = filter_audio_stats(request.args)
        return jsonify(stats)




# Data Parsing and Aggregation
def parse_dates(args):
    option = args.get('date_quick', None)
    if option:
        end_date = max_date
        if option == "0":
            start_date = end_date - dt.timedelta(days=7)
        elif option == "1":
            start_date = end_date - dt.timedelta(days=31)
        else:
            start_date = min_date
    else:
        s = args['date_range'].split('_')
        start_date = dt.datetime.strptime(s[0], "%Y-%m-%d")
        end_date = dt.datetime.strptime(s[1], "%Y-%m-%d")
        
    return start_date, end_date
    

def filter_bar(tab, lang, args):

    start, end = parse_dates(args)
    data_col = tab_to_col[tab]
    frame = df[(df.date >= start) & (df.date <= end)][['date', 'tour', 'lang', data_col]]
    if int(lang):
        frame = frame[frame.lang == num_to_lang[lang]]
    else:
        if tab == "1": # completion ratio
            frame = frame.groupby(['date', 'tour'])[data_col].mean().reset_index()
        else: # 
            frame = frame.groupby(['date', 'tour'])[data_col].sum().reset_index()

    frame = frame.groupby('tour')[data_col].mean().reset_index()
    frame.columns = ['Item', 'Value']
    frame['Value'] = frame['Value'].map(lambda x: round(x, 1))
    frame = rename_xlabels(frame)
    return frame

def filter_pie(tour_filter, args):
    start, end = parse_dates(args)
    frame = df[(df.date >= start) & (df.date <= end)] # Filter dates
    frame = frame[df.tour == tour_filter].groupby('lang')['tour_start'].mean().reset_index()
    total_tours = frame.tour_start.sum()
    frame['percent'] = frame['tour_start'].map(lambda x: round(x/total_tours*100, 1))
    frame.columns = ['label', 'value', 'percent']
    frame['value'] = frame['value'].map(lambda x: round(x, 1))
    # Sort languages
    lang_order = ['en', 'nl', 'fr', 'es', 'it', 'de', 'zh', 'ja', 'ru']
    extra_df = []
    for lang in lang_order:
        if lang not in frame.label.to_list():
            extra_df.append([lang, 0, 0])
    print(extra_df)
    if len(extra_df):
        new_frame = frame.iloc[:,:]
        new_frame = pd.concat([new_frame, pd.DataFrame(extra_df, columns=['label', 'percent', 'value'])])
        frame = new_frame


    frame.label = pd.Categorical(frame.label, lang_order)
    frame = frame.sort_values(by=['label'])
    frame.label = frame.label.map(lambda x: expand_lang[x])
    print(frame)
    return frame

def sum_switches(sr):
    agg = sr.iloc[0].copy()
    for ary in sr.iloc[1:]:
        agg += ary
    return str(agg)

def filter_stats(args):
    # Work out this logic
    # Make work for empty tours -> Set everything to zero

    try:

        start, end = parse_dates(args)
        tour_filter = num_to_tour[request.args['tour_index']]
        
        # # Filter Dates and Tour
        frame = df[(df.date >= start) & (df.date <= end)]
        frame = frame[frame.tour == tour_filter]
        frame_media = df_media[df_media.tour == tour_filter]

        # Filter Language
        lang = num_to_lang[args['lang']].strip()
        if lang == 'all':
            frame = frame.groupby(['date', 'tour']).agg({'tour_start':'sum', 
            'is_museum':'sum', 
            'audio_cr':'mean',
            'tour_switch': sum_switches})
            frame.tour_switch = frame.tour_switch.map(lambda x: np.array(list(map(int, x[1:-1].split()))))
            print(frame.tour_switch)
            print('Got here')

        else:
            frame = frame[frame.lang == lang]
            frame_media = frame_media[df_media.lang == lang]



        tour_mean = round(frame.tour_start.mean(), 0)
        media_cr = round(frame.audio_cr.mean(),2)

        # Compute museum ratio
        is_museum = round((frame.is_museum/frame.tour_start).mean(),2)
        if is_museum is np.nan:
            is_museum = 0

        # Tour Switches
        agg = frame.tour_switch.iloc[0].copy()
        for ary in frame.tour_switch.iloc[1:]:
            agg += ary
        switched_tour = tour_abbr[tour_order_list[np.argmax(agg)]]
        tour_switch_total = agg.sum()

        avg_media_length = round(frame_media.total_main_audio_length.iloc[0] / (frame_media.num_stops.iloc[0] + 2), 1)
        main_audio_cr = round(frame.audio_cr.mean(),2)
        room_sit = 'WC'
    except:
        tour_mean = 0
        tour_switch_total = 0
        switched_tour = "N/A"
        avg_media_length = 0
        main_audio_cr = 0
        is_museum = 0
        room_sit = 'N/A'

    result = {'tour_start': str(tour_mean), 
            'tour_switch': str(tour_switch_total), 
            'switch_target': str(switched_tour), 
            'media_len': str(avg_media_length),
            'media_com': str(main_audio_cr),
            'room_sit':room_sit,
            'museum': str(is_museum),
    }
    return result

def filter_audio_stats(args):
    result = {}

    # Filter Dates and Tour
    frame = df[(df.date >= start) & (df.date <= end)]
    frame = frame[frame.tour == tour_filter]
    frame_media = df_media[df_media.tour == tour_filter]

    # Language Filter
    # To-Do

    # Tour Media Stats
    audio_len = frame_media.total_main_audio_length.iloc[0]
    avg_clip_length = frame_media.total_main_audio_length.iloc[0] / (frame_media.num_stops.iloc[0] + 2)
    num_stops = frame_media.num_stops.iloc[0]

    # History Stats

    

    result['audio_len'] = audio_len
    result['avg_clip_length'] = avg_clip_length
    result['num_stops'] = num_stops
    return result


def aggregate_lang(df):
    pass

def rename_xlabels(df):
    order = [k for k,v in tour_abbr.items()]
    df.Item = pd.Categorical(df.Item, order)
    df.sort_values('Item', inplace=True)
    df.Item = df.Item.map(lambda x: tour_abbr[x])
    return df

def prepare_csv(df):
    csv_obj = StringIO()
    df.to_csv(csv_obj, index=False)
    csv_ready = csv_obj.getvalue()
    csv_obj.close()
    return csv_ready

if __name__ == "__main__":
    app.run(host='0.0.0.0', use_reloader = True)
