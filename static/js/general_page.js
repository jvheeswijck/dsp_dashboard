var base_url = "http://localhost:5000";
var current_tab = 0;
callback_args = {};

var bar_ylabels = {
    0: "Average Daily Number of Tour Starts",
    1: "Average % Completion per Media Clip",
    2: "",
    3: "",
};

var bar_titles = {
    0: "Popularity of Tours Based on Number of Starts",
    1: "Average Completion % per Media Clip for Tours",
    2: "",
    3: ""
};

$('.nav-item.main-nav[value=0]').addClass('active');

$('.nav-item.top-nav').click(function () {
    $(".nav-item.top-nav.active").toggleClass("active");
    $(this).toggleClass("active");
    var nav_element = ($(this).data('val'));
    current_tab = nav_element;
    address = make_general_request(nav_element);
    setFilterCard(current_tab);
    update_bars(address);
});

function general_bar_update() {
    address = make_general_request(current_tab);
    update_bars(address);
}

function make_general_request(tab) {
    lang = $('#language-filter').val();
    date_request = format_date_request();
    formed_address = `${base_url}/data?g=gen_data&tab=${tab}&lang=${lang}&${date_request}`
    return formed_address;
}

function format_date_request() {
    if (current_date_picker == 0) {
        date_request_format = "date_quick=" + String($('.date-select-quick:checked').val())
    } else {
        start_date = $('input[name="dates"]').data('daterangepicker').startDate.format("YYYY-MM-DD")
        end_date = $('input[name="dates"]').data('daterangepicker').endDate.format("YYYY-MM-DD")
        date_request_format = `date_range=${start_date}_${end_date}`;
    };
    return date_request_format
}

// Data Filter Callbacks
quick_date_callback = general_bar_update;
range_date_callback = general_bar_update;
language_callback = general_bar_update;

function update_bars(data_path) {
    console.log("Updating Bars")
    // Delay this a bit
    d3.select('#ylabel').transition().delay(400).text(bar_ylabels[current_tab]);
    // $('#ylabel').text(bar_ylabels[current_tab]);
    $('#charttitle').text(bar_titles[current_tab]);

    d3.csv(data_path, function (data) {

        // Find new y-domain
        max_value = 0;
        for (var i = 0; i < d3.keys(data).length; i++) {
            var temp = Number(d3.values(data)[i].Value);
            if (temp > max_value) {
                max_value = temp;
            };
        }
        if ([1].includes(current_tab)) {
            max_value = 100
        }
        // if (ratio){
        //     max_value = 100
        // }

        // Set scaling
        var x = d3.scaleBand()
            .range([0, width])
            .domain(data.map(function (d) { return d.Item; }))
            .padding(0.2);

        var y = d3.scaleLinear()
            .domain([0, max_value])
            .range([height, 0]);


        svg.select(".yaxis")
            .transition()
            .call(d3.axisLeft(y))
        // d3.select("#ylabel")
        //     .text(bar_ylabels[index]);

        svg.select(".xaxis")
            .transition()
            .call(d3.axisBottom(x))
            .selectAll("text")
            .attr("transform", "translate(-10,0)rotate(-45)")
            .style("text-anchor", "end");

        // svg.select(".yaxis")
        // .transition()
        // .call(d3.axisLeft(y))

        // Animate Bars
        var u = svg.selectAll("rect")
            .data(data);

        u
            .enter()
            .append("rect")
            .on("mouseover", handleMouseOver)
            .on("mouseout", handleMouseOut)
            .on("click", handleBarClick)
            .merge(u)
            .transition()
            .duration(600)
            .attr("x", function (d) { return x(d.Item); })
            .attr("y", function (d) { return y(d.Value); })
            .attr("width", x.bandwidth())
            .attr("height", function (d) { return height - y(d.Value); })
            .attr("fill", "#69b3a2");


        u.exit()
            .remove();

    })
}

function setFilterCard(nav_index) {
    // console.log("SETTING CARD")
    if (nav_index == 2) {

        $("#date-filter-element").addClass("hide-element");
    } else {
        $("#date-filter-element").removeClass("hide-element");
        $("#language-filter-element").removeClass("hide-element");
    };
};


// ------------ Bar MouseOver Functions--------------- //

var opacity = 1;
var opacityHover = 0.8;
var otherOpacityOnHover = .8;
var tooltipMargin = rem;

function handleMouseOver(d, i) {
    d3.select(this).attr('fill', bar_color_hover);
    e = d3.select(this);
    draw_tooltip(d, i, e);
};

function handleMouseOut(d, i) {
    // Use D3 to select element, change color back to normal
    d3.select(this).attr('fill', bar_color_normal);
    d3.select("svg")
        .select(".tooltip").remove();
};


function draw_tooltip(d, i, e) {
    console.log("Drawing");
    console.log(e);
    let g = d3.select("#graph")
        .append("g")
        .attr("class", "tooltip")
        .style("opacity", 0);

    g.append("text")
        .attr("class", "name-text")
        .text(`${d.Value}`)
        .attr("x", e.attr('x'))
        .attr("y", e.attr('y'))
        .style('fill', 'black')
        .attr('text-anchor', 'middle');

    let text = g.select("text");
    text.attr('transform', `translate(${e.attr('width') / 2}, ${-tooltipMargin})`);

    d3.select('.tooltip')
        .style("opacity", 1)
};

function handleBarClick(d, i) {
    window.location = "http://localhost:5000/tours?tour_index=" + String(i);
}


function post_load_callback(args) {

};