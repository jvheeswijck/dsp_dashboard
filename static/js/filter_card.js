current_date_picker = 0;
first_date = 0;
last_date = 0;

// Get and set dates
d3.json('http://localhost:5000/data?g=date_range', function (data) {
    first_date = data.first;
    last_date = data.last;

    // Load Date Picker
    $('input[name="dates"]').daterangepicker({
        "minDate": first_date,
        "maxDate": last_date
    });
    $('input[name="dates"]').on('apply.daterangepicker', function (ev, picker) {
        range_date_callback(callback_args);
    });
});


// Handle Date Selector Toggle
$('.date-select-options').click(function () {
    var date_select_option = $(this).val();
    console.log("Switching Toggle")
    setDatePickerInterface(date_select_option);
});

// On Quick Date Change
$('.date-select-quick').click(function () {
    var quick_option = $(this).val();
    quick_date_callback(callback_args);
})

// On Language Change
$(document).on('change', '#language-filter', function () {
    var lang_value = $(this).val();
    language_callback(callback_args);
});

function setDatePickerInterface(val) {
    if (current_date_picker != val) {
        var row_element = $('.date-picker-view');
        // row_element.toggleClass('hide-element');
        $('#date-picker-select').toggleClass('hide-element');
        $('#date-quick-select').toggleClass('hide-element');
        // row_element.toggleClass('hide-element');
    };
    current_date_picker = val;
};

// Final Load Callbacks
console.log('Final Callbacks')
post_load_callback(callback_args);

