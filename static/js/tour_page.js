// var current_tour = "";
// var current_tab = ;
// var tour_lang = "";

var current_sub_nav = 0;
var current_date_picker = 0;
var callback_args = { 'tour': current_tour, 'sub_nav': current_sub_nav };
var base_url = "http://localhost:5000/data/";
var info_card_text = {};

var rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
var margin = { top: 4 * rem, right: 2 * rem, bottom: 6 * rem, left: 4 * rem };
var width = document.getElementById('graph_card_tour').offsetWidth - margin.left - margin.right;
var height = document.getElementById('graph_card_tour').offsetHeight - margin.bottom - margin.top - 2 * rem;

// Set Info Card
d3.json("http://localhost:5000/data?g=info&page=tour", function (data) {
    info_card_text = data;
    setInfoCard();
});

function setInfoCard() {
    var card_content = $('#info_card_content');
    card_content.empty();
    card_content.append(info_card_text[current_sub_nav]);
}

// ------------ Initialize--------------- //

// Set active top-nav
$(".nav-item.sub-nav[value=" + String(current_tour) + "]").toggleClass('active');

// Set active main-nav
$('.nav-item.main-nav[value=1]').addClass('active');

// ------------ Navigation --------------- //
// Handle tour navigation
$('.nav-item.sub-nav').click(function () {
    // Nav Styling
    $(".nav-item.sub-nav.active").toggleClass("active");
    $(this).toggleClass("active");

    // Refresh Content
    let nav_element = ($(this).val());
    refreshContent(nav_element, current_sub_nav);
    current_tour = nav_element;
});

// Handle metric-navigation
$('.nav-item.top-nav').click(function () {
    // Nav Styling
    $(".nav-item.top-nav.active").toggleClass("active");
    $(this).toggleClass("active");

    // Refresh Content
    var clicked_sub_nav = ($(this).val());
    setFilterCard(clicked_sub_nav);
    console.log(current_tour, clicked_sub_nav)
    refreshContent(current_tour, clicked_sub_nav);
    current_sub_nav = clicked_sub_nav;
    setInfoCard();
});

function refreshContent(tour, sub_nav) {
    console.log('Loading...');
    callback_args = { 'tour': tour, 'sub_nav': sub_nav };

    if (tour == current_tour) {
        if (sub_nav != current_sub_nav) {
            draw_methods[sub_nav](callback_args);
            setFilterCallbacks(sub_nav)
        };
    } else {
        if (sub_nav == current_sub_nav) {
            update_methods[sub_nav](callback_args);
        };
    };
};

function setFilterCallbacks(index) {
    quick_date_callback = update_methods[index];
    range_date_callback = update_methods[index];
    language_callback = update_methods[index];
    
    if (index == 2){ // Temporary Fix
        quick_date_callback = updateLanguage;
        range_date_callback = updateLanguage;
        language_callback = updateLanguage;
        }
}

// ------------ Draw & Update Content --------------- //
function drawStats(args) {
    console.log('Drawing stats');
    $('.graph-wrapper').empty();
    $('.stats-wrapper').removeClass('hide-element');
    $('.graph-wrapper').addClass('hide-element');
    updateStats(args)
};

function updateStats(args) {
    console.log('Updating stats');
    address = makeStatsRequest(args.tour, args.sub_nav);
    speed_transition = 500
    d3.json(address, function (data) {


        d3.select('.stats-box[value="tour_start"] p')
            .data([data['tour_start']])
            .transition()
            .duration(speed_transition)
            .textTween(tweenNumber);

        d3.select('.stats-box[value="tour_switch"] p')
            .data([data['tour_switch']])
            .transition()
            .duration(speed_transition)
            .textTween(tweenNumber);

        d3.select('.stats-box[value="switch_target"] p')
            .data([data['switch_target']])
            .transition()
            .duration(speed_transition)
            .text((d) => d);

        d3.select('.stats-box[value="media_len"] span')
            .data([data['media_len']])
            .transition()
            .duration(speed_transition)
            .textTween(tweenNumber);

        // d3.select('.stats-box[value="media_lis"] span.stat-value')
        // .data([data['media_lis']])
        // .transition()
        // .duration(speed_transition)
        // .textTween(tweenNumber);

        // $('.stats-box[value="media_lis"] p span').text((i,s) => s + '%');

        d3.select('.stats-box[value="media_com"] span')
            .data([data['media_com']])
            .transition()
            .duration(speed_transition)
            .textTween(tweenNumber);

        d3.select('.stats-box[value="museum"] p')
            .data([data['museum']])
            .transition()
            .duration(speed_transition)
            .textTween(tweenNumber);


        d3.select('.stats-box[value="room_sit"] p')
            .data([data['room_sit']])
            .transition()
            .duration(speed_transition)
            .text((d) => d);


        // Non-animating
        // $('.stats-box').each(function(i,d) {
        //     new_value = data[$(this).attr('value')]
        //     $(this).children('p').text(new_value);
        // });


    });
};

function makeStatsRequest(tour) {
    lang = $('#language-filter').val();
    date_request = format_date_request();
    formed_request = `http://localhost:5000/data?g=stats&tour_index=${tour}&lang=${lang}&${date_request}`;
    return formed_request
};



// Audio Stats
function drawAudioStats(args) {
    console.log('Drawing Audio Stats');
    $('.graph-wrapper').empty();
    $('.stats-wrapper').addClass('hide-element');
    $('.graph-wrapper').addClass('hide-element');
    $('.stats-audio-wrapper').addClass('hide-element');
    updateAudioStats(args)
};

function updateAudioStats(args) {

};

function makeAudioStatsRequest(tour) {

};

function drawRoomDur(tour, sub_nav) {
    $('.stats-wrapper').addClass('hide-element');
    $('.graph-wrapper').removeClass('hide-element');


    card_element = $('.graph-wrapper');
    card_element.empty();

    console.log('Drawing room');
    console.log(tour, sub_nav);
};

function updateRoomDur(tour, sub_nav) {
    console.log('Updating room');

};

function mergeWithFirstEqualZero(first, second) {

    var secondSet = d3.set();

    second.forEach(function (d) { secondSet.add(d.label); });

    var onlyFirst = first
        .filter(function (d) { return !secondSet.has(d.label) })
        .map(function (d) { return { label: d.label, value: 0 }; });

    var sortedMerge = d3.merge([second, onlyFirst])
        .sort(function (a, b) {
            return d3.ascending(a.label, b.label);
        });

    return sortedMerge;
};

// Draw Pie Chart for Language

keys = ['English', 'Dutch', 'French', 'Spanish', 'Italian', 'German', 'Chinese', 'Japanese', 'Russian'];
var radius = Math.min(width, height) / 2;
var arc = d3.arc()
    .innerRadius(radius - radius * 0.1)
    .outerRadius(radius - radius * 0.4);
var color = d3.scaleOrdinal(d3.schemeTableau10)
    .domain(keys);
var pie = d3.pie()
    .value((d) => d.value)
    .padAngle(.005)
    .sort(null);

function arcTween(d) {
    // Version 1
    // var i = d3.interpolate(this._current, d);
    // this._current = i(0);
    // return function (t) {
    //     return arc(i(t));
    // };

    // Version 2
    var interpolate = d3.interpolate(this._current, d);
    var _this = this;
    return function (t) {
        _this._current = interpolate(t);
        return arc(_this._current);
    };

};

function exitTween(d) {
    var end = Object.assign({}, this._current, { startAngle: this._current.endAngle });
    var i = d3.interpolate(d, end);
    return function (t) {
        return arc(i(t));
    };
}


function drawLanguage(args) {
    console.log("Drawing Languages")
    $('.stats-wrapper').addClass('hide-element');
    $('.graph-wrapper').removeClass('hide-element');
    $('.graph-wrapper').empty();

    let tour = args.tour;


    // append the svg object to the body of the page
    var svg = d3.select(".graph-wrapper")
        .append("svg")
        .attr("style", "height: 100%")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .attr("id", "graph")
        .append("g")
        .attr("id", "arc-group")
        .attr("transform", `translate(${(width + margin.left + margin.right) / 2},${(height + margin.top + margin.bottom) / 2})`);

    var radius = Math.min(width, height) / 2;
    // var color = d3.scaleOrdinal(d3.schemeCategory20);

    // .startAngle(-90 * Math.PI / 180)
    // .endAngle(-90 * Math.PI / 180 + 2 * Math.PI)


    // Title
    let title = $('.nav-item.top-nav.active a').text();
    d3.select('svg').append("text")
        .attr('text-anchor', 'middle')
        .attr('id', 'chart-title')
        .attr('x', '50%')
        .attr('y', rem * 3)
        .text(`Percentage of Langauge Usage for ${title} Tour`);

    d3.csv(make_pie_address(tour), function (data) {

        var path = svg.selectAll("path")
            .data(pie(data))

        path.enter()
            .append("path")
            .style("fill", function (d, i) { return color(d.data.label); })
            .transition()
            .duration(400)
            .attr("d", arc)
            .attrTween('d', function (d) {
                var i = d3.interpolate(d.startAngle + 0.1, d.endAngle);
                return function (t) {
                    d.endAngle = i(t);
                    return arc(d);
                }
            })
            .each(function (d) { this._current = d; }); // store the initial angles

        // function arcTween(a) {
        //     // console.log('Tweening');
        //     var i = d3.interpolate(this._current, a);
        //     this._current = i(0);
        //     return function (t) {
        //         return arc(i(t));
        //     };
        // };


        var legendItemSize = 18
        var legendSpacing = 4

        var legend = svg
            .selectAll('.legend')
            .data(data)
            .enter()
            .append('g')
            .attr('class', 'legend')
            .attr('transform', (d, i) => {
                var height = legendItemSize + legendSpacing
                var offset = height * color.domain().length / 2
                var x = legendItemSize * -2;
                var y = (i * height) - offset
                return `translate(${x}, ${y})`
            })

        legend
            .append('rect')
            .style('fill', (d, i) => color(d.label))
            .transition()
            .duration(400)
            .attr('width', legendItemSize)
            .attr('height', legendItemSize)
            .delay(function (d, i) { return (i * 25) });

        legend
            .append('text')
            .attr('x', legendItemSize + legendSpacing)
            .attr('y', legendItemSize - legendSpacing)
            .transition()
            .duration(400)
            .text((d) => d.label)
            .delay(function (d, i) { return (i * 25) });

        svg.selectAll("path")
            .on("mouseover", handleMouseOverPie)
            .on("mouseout", handleMouseOutPie)
            .on("mousemove", handleMouseOverMove);
    });
};

function updateLanguage(args) {
    console.log("Updating Pie Chart");

    let tour = args.tour;
    var duration = 400;
    // var key = function (d) { return d.data.label; };

    // var pie = d3.pie()
    //     // .value(function (d) { return d.value; })
    //     .padAngle(.005)
    //     .sort(null);
    // .startAngle(-90 * Math.PI / 180)
    // .endAngle(-90 * Math.PI / 180 + 2 * Math.PI)



    let title = $('.nav-item.top-nav.active a').text();
    d3.select('#chart-title')
        .text(`Percentage of Langauge Usage for ${title} Tour`);


    function key(d) { return d.data.label; };

    d3.csv(make_pie_address(tour), function (data) {

        svg = d3.select('#arc-group');

        function arcTween(a) {
            var i = d3.interpolate(this._current, a);
            this._current = i(0);
            return function (t) {
                return arc(i(t));
            };
        };


        // var path = svg.datum(data).selectAll("path")
        //     .data(pie)
        //     .enter().append("path")
        //     .attr("fill", function (d, i) { return color(d.label); })
        //     .attr("d", arc)
        //     .each(function (d) { this._current = d; }); // store the initial angles


        // function change() {
        //     var value = this.value;
        //     pie.value(function (d) { return d[value]; }); // change the value function
        //     path = path.data(pie); // compute the new angles
        //     path.transition().duration(750).attrTween("d", arcTween); // redraw the arcs
        // }
        var duration = 400;

        var oldData = d3.selectAll("path")
            .data().map(function (d) { return d.data });

        if (oldData.length == 0) oldData = data;

        var was = mergeWithFirstEqualZero(data, oldData);
        var is = mergeWithFirstEqualZero(oldData, data);

        var path = svg.selectAll("path")
            .data(pie(was), key)

        // var slice = d3.select('svg g').selectAll("path")
        //     .data(pie(was), key);


        // slice.enter()
        //     .insert("path")
        //     .attr("class", "slice")
        //     .style("fill", function (d) { return color(d.data.label); })
        //     .each(function (d) {
        //         this._current = d;
        //     });


        path.enter()
            .append("path")
            .style("fill", (d) => color(d.data.label))
            .each(function (d) {
                this._current = d;
            });

        oldSlices = path.exit();
        oldSlices.transition()
            .duration(400)
            .attrTween('d', exitTween)
            .remove();


        var path = svg.selectAll("path")
            .data(pie(is), key)

        // slice = svg.selectAll("path")
        //     .selectAll("path")
        //     .data(pie(is), key);

        path.transition()
            .duration(400)
            .attrTween("d", function (d) {
                var interpolate = d3.interpolate(this._current, d);
                var _this = this;
                return function (t) {
                    _this._current = interpolate(t);
                    return arc(_this._current);
                };
            });

        // slice = d3.select('svg g').selectAll("path")
        //     .selectAll("path")
        //     .data(pie(data), key)

        // var newSlices = slices.enter().append('path')
        // .each(function(d) { this._current = Object.assign({}, d, { startAngle: d.endAngle }); })
        // .attr('class', 'slice')
        // .style('fill', function (d) { return color(joinKey(d)); });



        // slice.exit()
        //     .transition()
        //     .delay(500)
        //     .duration(duration)
        //     .remove()
        //     .on("mouseover", handleMouseOverPie)
        //     .on("mouseout", handleMouseOutPie)
        //     .on("mousemove", handleMouseOverMove);

        // d3.select('.arc-group').selectAll("path")
        //     .on("mouseover", handleMouseOverPie)
        //     .on("mouseout", handleMouseOutPie)
        //     .on("mousemove", handleMouseOverMove);


        // Update Arcs
        // let duration = 400;

        var path = svg.selectAll("path")
            .data(pie(data))

        // path = svg.seleect('svg g').selectAll("path")
        //     .data(pie(data))


        // var path = svg.selectAll("path")
        // .data(pie(data))

        // path.enter()
        //     .append("path")
        //     .transition()
        //     .duration(400)
        //     .attr("fill", function (d, i) { return color(d.data.label); })
        //     .attr("d", arc)
        //     .each(function (d) { this._current = d.value; }) // store the initial angles
        //     .attrTween('d', function (d) {
        //         var i = d3.interpolate(d.startAngle + 0.1, d.endAngle);
        //         return function (t) {
        //             d.endAngle = i(t);
        //             return arc(d);
        //         };

        // path.exit()
        //     .transition()
        //     .delay(0)
        //     .duration(0)
        //     .remove();


        // enter = path.enter()
        //     .append("path")
        //     .on("mouseover", handleMouseOverPie)
        //     .on("mouseout", handleMouseOutPie)
        //     .on("mousemove", handleMouseOverMove)
        //     .attr("d", arc)
        //     .attr("fill", (d, i) => color(d.data.label))
        //     .each(function (d) { this._current = d; }) // store the initial angles
        //     ;
        // // .attrTween("d", arcTween);

        // path.merge(enter);

        // path.transition()
        //     .duration(duration)
        //     .attrTween("d", arcTween);
        //      // redraw the arcs


        // Update Legend
        var legendItemSize = 18
        var legendSpacing = 4



        u = svg
            .selectAll('.legend')
            .data(data);

        // svg
        //     .selectAll('.legend')
        //     .data(data)
        //     .exit()
        //     .remove();
        u.exit()
            .remove()

        var legend = u.enter()
            .append('g')
            // .merge(u)
            .attr('class', 'legend')
            .attr('transform', (d, i) => {
                var height = legendItemSize + legendSpacing
                var offset = height * color.domain().length / 2
                var x = legendItemSize * -2;
                var y = (i * height) - offset
                return `translate(${x}, ${y})`
            })


        legend
            .append('rect')
            .style('fill', (d, i) => color(d.label))
            .transition()
            .duration(400)
            .attr('width', legendItemSize)
            .attr('height', legendItemSize)
            .delay(function (d, i) { return (i * 25) });

        legend
            .append('text')
            .attr('x', legendItemSize + legendSpacing)
            .attr('y', legendItemSize - legendSpacing)
            .transition()
            .duration(400)
            .text((d) => d.label)
            .delay(function (d, i) { return (i * 25) });

        // legend_elements.merge(legend)

        //     .data(data)



        // enter = legend.enter()
        //     .append('g')
        //     .attr('class', 'legend')
        //     .attr('transform', (d, i) => {
        //         var height = legendItemSize + legendSpacing
        //         var offset = height * color.domain().length / 2
        //         var x = legendItemSize * -2;
        //         var y = (i * height) - offset
        //         return `translate(${x}, ${y})`
        //     });

        // legend.merge(enter);


        // legend
        //     .append('rect')
        //     .style('fill', (d, i) => color(d.label))
        //     .transition()
        //     .duration(400)
        //     .attr('width', legendItemSize)
        //     .attr('height', legendItemSize)
        //     .delay(function (d, i) { return (i * 25) });

        // legend
        //     .append('text')
        //     .attr('x', legendItemSize + legendSpacing)
        //     .attr('y', legendItemSize - legendSpacing)
        //     .transition()
        //     .duration(400)
        //     .text((d) => d.label)
        //     .delay(function (d, i) { return (i * 25) });

    });
};

function make_pie_address(tour) {
    date_request = format_date_request();
    formed_request = `http://localhost:5000/data?g=pie&tour_index=${tour}&${date_request}`;
    return formed_request
}




function makeRoomRequest() {
    lang = $('#language-filter');

};

// This is temporary -> move to another file
function format_date_request(init = false) {

    if (current_date_picker == 0) {
        date_request_format = "date_quick=" + String($('.date-select-quick:checked').val())
    } else {
        start_date = $('input[name="dates"]').data('daterangepicker').startDate.format("YYYY-MM-DD")
        end_date = $('input[name="dates"]').data('daterangepicker').endDate.format("YYYY-MM-DD")
        date_request_format = `date_range=${start_date}_${end_date}`;
    };
    return date_request_format
}

// ------------ Update Logic --------------- //
// Set content card methods
var draw_methods = [drawStats, drawRoomDur, drawLanguage];
var update_methods = [updateStats, updateRoomDur, updateLanguage];


// Data Filter Callbacks
quick_date_callback = update_methods[0];
range_date_callback = update_methods[0];
language_callback = update_methods[0];

// Adjust Data Filter Card Options
function setFilterCard(nav_index) {
    // console.log("SETTING CARD")
    if (nav_index == 2) {
        if (nav_index != current_sub_nav) {
            $("#language-filter-element").addClass("hide-element");
        }
    } else {
        $("#language-filter-element").removeClass("hide-element");
    };
};

$(document).on('change', '#language-filter', function () {
    var select_value = $(this).val();
    refreshContent(current_tour, current_sub_nav);
});

// ------------ Pie MouseOver Functions--------------- //

var opacity = 1;
var opacityHover = 0.8;
var otherOpacityOnHover = .8;
var tooltipMargin = rem;

function handleMouseOverPie(d, i) {
    d3.select(this)
        .style("opacity", opacityHover);

    let g = d3.select("svg")
        .append("g")
        .attr("class", "tooltip")
        .style("opacity", 0);

    g.append("text")
        .attr("class", "name-text")
        .text(`${d.data.label} | ${d.data.value} (${d.data.percent}%)`)
        .attr('text-anchor', 'middle')
        .style('fill', 'black');

    let text = g.select("text");
    let bbox = text.node().getBBox();
    let padding = 4;
    let mousePosition = d3.mouse(this);

    g.insert("rect", "text")
        .attr("x", bbox.x - padding)
        .attr("y", bbox.y - padding)
        .attr("width", bbox.width + (padding * 2))
        .attr("height", bbox.height + (padding * 2))
        .style("fill", "LightBlue")
        .style("opacity", 0.80);
};


function handleMouseOverMove(d, i) {
    let mousePosition = d3.mouse(this);
    let w = width + margin.left + margin.right;
    let h = height + margin.bottom + margin.top;

    let x = mousePosition[0] + w / 2;
    let y = mousePosition[1] + h / 2 - tooltipMargin;



    let text = d3.select('.tooltip text');
    let bbox = text.node().getBBox();
    if (x - bbox.width / 2 < 0) {
        x = bbox.width / 2;
    }
    else if (w - x - bbox.width / 2 < 0) {
        x = w - bbox.width / 2;
    }

    if (y - bbox.height / 2 < 0) {
        y = bbox.height + tooltipMargin * 2;
    }
    else if (h - y - bbox.height / 2 < 0) {
        y = h - bbox.height / 2;
    }

    d3.select('.tooltip')
        .style("opacity", 1)
        .attr('transform', `translate(${x}, ${y})`);
};

function handleMouseOutPie(d, i) {
    d3.select("svg")
        .select(".tooltip").remove();
    d3.selectAll('path')
        .style("opacity", opacity);
};


function tweenNumber(d) {
    var i = d3.interpolate(this.textContent, d),
        prec = (d + "").split("."),
        round = (prec.length > 1) ? Math.pow(10, prec[1].length) : 1;

    return function (t) {
        return Math.round(i(t) * round) / round;
    };
};

var post_load_callback = drawStats;