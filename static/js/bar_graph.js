// set the dimensions and margins of the graph

rem = parseFloat(getComputedStyle(document.documentElement).fontSize);
var margin = { top: 4 * rem, right: 2 * rem, bottom: 6 * rem, left: 4 * rem };
width = document.getElementById('graph_card_general').offsetWidth - margin.left - margin.right;
height = document.getElementById('graph_card_general').offsetHeight - margin.bottom - margin.top - 2 * rem;

var bar_color_normal = "#69b3a2";
// var bar_color_normal = "#69b38ee5";
var bar_color_hover = '#427066';
var data_path = "http://localhost:5000/data?g=gen_data&lang=0&date_quick=0&tab=0";

// append the svg object to the body of the page
var svg = d3.select("#my_dataviz")
  .append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("id", "graph")
  .attr("transform",
    "translate(" + margin.left + "," + margin.top + ")");


// Parse the Data
d3.csv(data_path, function (data) {
  // Find max y-value
  max_value = 0;
  for (var i = 0; i < d3.keys(data).length; i++) {
    var temp = Number(d3.values(data)[i].Value);
    if (temp > max_value) {
      max_value = temp;
    };
  }

  // Title
  svg.append("text")
    .attr("x", (width / 2))
    .attr("y", 0 - (margin.top / 2))
    .attr("text-anchor", "middle")
    .attr("id", "charttitle")
    .style("font-size", "16px")
    .style("text-decoration", "underline")
    .text("Popularity of Tours Based on Number of Tours Starts");

  // X axis
  var x = d3.scaleBand()
    .range([0, width])
    .domain(data.map(function (d) { return d.Item; }))
    .padding(0.2);

  svg.append("g")
    .attr('class', 'xaxis')
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
    .selectAll("text")
    .attr("transform", "translate(-10,0)rotate(-45)")
    .style("text-anchor", "end");

  // text label for the x axis
  svg.append("text")
    .attr("transform",
      "translate(" + (width / 2) + " ," +
      (height + margin.top + 20) + ")")
    .attr("id", 'xlabel')
    .style("text-anchor", "middle")
    .text("Tour Name");


  // Add Y axis
  var y = d3.scaleLinear()
    .domain([0, Math.round(max_value * 1.05)])
    .range([height, 0]);

  svg.append("g")
    .attr("class", "yaxis")
    .call(d3.axisLeft(y));


  // text label for the y axis
  svg.append("text")
    .attr("transform", "rotate(-90)")
    .attr("y", 0 - margin.left + 0.5 * rem)
    .attr("x", 0 - (height / 2))
    .attr("dy", "1em")
    .attr("id", "ylabel")
    .style("text-anchor", "middle")
    .text("Number of Tour Starts");

  // Bars
  svg.selectAll("mybar")
    .data(data)
    .enter()
    .append("rect")
    .attr("x", function (d) { return x(d.Item); })
    .attr("width", x.bandwidth())
    .attr("fill", bar_color_normal)
    // no bar at the beginning thus:
    .attr("height", function (d) { return height - y(0); }) // always equal to 0
    .attr("y", function (d) { return y(0); })
    .style("cursor", "pointer")
    .on("mouseover", handleMouseOver)
    .on("mouseout", handleMouseOut)
    .on("click", handleBarClick);

  // Animation
  svg.selectAll("rect")
    .transition()
    .duration(400)
    .attr("y", function (d) { return y(d.Value); })
    .attr("height", function (d) { return height - y(d.Value); })
    .delay(function (d, i) { return (i * 50) })
})

// ------------ Bar MouseOver Functions--------------- //

var opacity = 1;
var opacityHover = 0.8;
var otherOpacityOnHover = .8;
var tooltipMargin = rem;

function handleMouseOver(d, i) {
  d3.select(this).attr('fill', bar_color_hover);
  e = d3.select(this);
  draw_tooltip(d, i, e);
};

function handleMouseOut(d, i) {
  // Use D3 to select element, change color back to normal
  d3.select(this).attr('fill', bar_color_normal);
  d3.select("svg")
    .select(".tooltip").remove();
};


function draw_tooltip(d, i, e) {
  console.log("Drawing");
  console.log(e);
  let g = d3.select("#graph")
    .append("g")
    .attr("class", "tooltip")
    .style("opacity", 0);

  g.append("text")
    .attr("class", "name-text")
    .text(`${d.Value}`)
    .attr("x", e.attr('x'))
    .attr("y", e.attr('y'))
    .style('fill', 'black')
    .attr('text-anchor', 'middle');

  let text = g.select("text");
  text.attr('transform', `translate(${e.attr('width') / 2}, ${-tooltipMargin})`);

  d3.select('.tooltip')
    .style("opacity", 1)
};

function handleBarClick(d, i) {
  window.location = "http://localhost:5000/tours?tour_index=" + String(i);
}



